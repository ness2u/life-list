module.exports = (function(){

   let winston = require('winston');
   let q = require('q');
   let todoTxt = require("./todo-txt.js");
   let config = require('./config.js');

   let defaultList = "lifelist";

   function getList(options) {
      if(!options){ options = {}; }
      if(!options.project){ options.project = []; }
      if(!options.context){ options.context = []; }

      listname = options.listname || defaultList;
      winston.info('getting list: ' + listname);
      winston.info('options: ' + JSON.stringify(options));

      let deferred = q.defer();
      config.load().then(function(config) {
         let folder = config.listDir;
         let filename = listname + '.txt';
         todoTxt.parse(folder + filename)
            .then(filterList(options))
            .then(function(list){
               deferred.resolve(list);
            });
      });
      return deferred.promise;
   }

   function saveList(listname, list){
      listname = listname || defaultList;

      let deferred = q.defer();
      config.load().then(function(config){
         let folder = config.listDir;
         let filename = listname + '.txt';
         console.log('saving list: ' + listname);

         todoTxt.save(list, folder + filename)
         .then(function(list){
            let d = q.defer();
            winston.info('saved ' + filename);
            d.resolve(list);
            return d.promise;
         }).then(function(list){
            deferred.resolve(list);
         });
      });
      return deferred.promise;
   }

   function filterList(options){
      winston.info("filtering on: " + JSON.stringify(options));

      let hash = {
         projects: {},
         contexts: {}
      };
      let ignoreProjectFilter = true;
      let ignoreContextFilter = true;
      let showAll = options.showAll;

      if (options.projects) {
         ignoreProjectFilter = options.projects.length == 0;
         for(let p of options.projects){
            p = p.replace('+', '');
            hash.projects[p] = true;
         }
      }
      if(options.contexts){
         ignoreContextFilter = options.contexts.length == 0;
         for(let c of options.contexts){
            c = c.replace('@','');
            hash.contexts[c] = true;
         }
      }

      return function(list){
         let defer = q.defer();

         // exclude tasks on completion-status
         list = list.filter(function(item) {
            let predicate = (showAll || !item.completed);
            return predicate;            
         });

         // include only those matching a project
         list = list.filter(function(item) {
            for(let p of item.projects){
               if(hash.projects[p]){ return true; }
            }
            return ignoreProjectFilter;
         });

         // include only those matching a context
         list = list.filter(function(item) {
            for(let c of item.contexts){
               if(hash.contexts[c]){ return true; }
            }
            return ignoreContextFilter;
         });

         defer.resolve(list);
         return defer.promise;
      };
}

   function addTask(options) {
      listname = options.listname || defaultList;
      winston.info('adding task to ' + listname + ' with text: ' + options.text);

      let deferred = q.defer();
      getList({listname, project:[], context:[], showAll: true }).then(function(list){

         winston.info('loaded ' + listname + ': ' + list.length + ' tasks.');

         let tasks = todoTxt.parseText(options.text);
         for(let task of tasks) {
            winston.debug('adding task: ' + JSON.stringify(task));
            list.push(task);
         }

         saveList(listname, list).then(function(list){
            deferred.resolve(list);
         });
      });
      return deferred.promise;
   }

   function listContexts(options){
      listname = options.listname || defaultList;
      winston.info('listing contexts in list: ' + listname);

      let deferred = q.defer();
      getList({listname}).then(function(list){
         let context = selectMany(list, function(item){ return item.contexts; });
         winston.debug('contexts: ' + context);
         deferred.resolve(context);
      });

      return deferred.promise;
   }

   function listProjects(options){
      listname = options.listname || defaultList;
      winston.info('listing projects in list: ' + listname);

      let deferred = q.defer();
      getList({listname}).then(function(list){
         let projects = selectMany(list, function(item){ return item.projects; });
         winston.debug('projects: ' + projects);
         deferred.resolve(projects);
      });

      return deferred.promise;
   }

   function selectMany(list, selector){

      let hash = {};
      for(let item of list){
         let set = selector(item);
         for(let value of set){
            hash[value] = 1;
         }
      }

      let unique = [];
      for(let value in hash){
         unique.push(value);
      }

      return unique;
   }

   return {
      getList: getList,
      addTask: addTask,
      listProjects: listProjects,
      listContexts: listContexts
   };

})();
