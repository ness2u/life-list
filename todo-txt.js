module.exports = (function(){
   // this module parses and writes todo.txt files

   let winston = require('winston');
   let fs = require('fs');
   let q = require('q');

   // initializes an empty task
   function create(text){
      return {
         raw: "",
         completed: false,
         priority: null,
         dateCompleted: null,
         dateCreated: null,
         text: text,

         contexts: [],
         projects: [],

      };
   }

   function parse(filename) {
      let deferred = q.defer()

      winston.info('parsing file: ' + filename);
      fs.readFile(filename, 'utf8', function(err, data) {
         if(err) { 
            console.log('err: ' + err);
            deferred.reject(err);
            throw err;
         }

         let list = parseText(data);
         deferred.resolve(list);
      });

      return deferred.promise;
   }

   function save(list, filename){
      let deferred = q.defer();

      winston.info('saving file: ' + filename);
      let data = serializeList(list).join('\n');

      fs.writeFile(filename, data, function(err){
         if(err) {throw err;}
         deferred.resolve(list);
      });

      return deferred.promise;
   }

   function serializeList(list) {
      let data = list.map(serializeTask);
      return data;
   }

   function serializeTask(task){
      let data = '';

      if (task.completed) {
         data += 'x ';
         if (task.dateCompleted){
            data += '' + task.dateCompleted + ' ';
         }
      }


      if (task.priority) {
         data += '(' + task.priority + ') ';
      }
      if (task.dateCreated) {
         data += task.dateCreated + ' ';
      }
      data += task.text;

      return data;
   }

   function parseText(text){

      winston.debug("parsing text: " + text);
      text = text.replace(/\r/, ''); // strip windows carriage return
      let lines = text.split('\n')

      let tasks = [];
      for(let line of lines) {
         var task = parseTask(line);
         if(task){
            tasks.push(task);
         }
      }
      return tasks;
   }

   function parseTask(line){

      let pattern = /(?:(x)\s(?:(\d{4}-\d{2}-\d{2})\s)?)?(?:\((\w+)\)\s)?(?:(\d{4}-\d{2}-\d{2})\s)?(.+)/;
      let projectsPattern = /\+([\w-]+)/g;
      let contextsPattern = /@([\w-]+)/g;

      winston.debug('parsing line: ' + line);

      let matches = pattern.exec(line);
      if (!matches){
         winston.debug('no matches, returning null task');
         return null;
      }

      let projects = [];
      let contexts = [];
      let task = {
         raw:line,
         projects,
         contexts
      };
      task.completed = matches[1] ? true : false;
      task.dateCompleted = matches[2];
      task.priority = matches[3];
      task.dateCreated = matches[4];
      task.text = matches[5];
      task.cleartext = task.text.replace(projectsPattern, '').replace(contextsPattern, '').trim();

      if(!task.text){
         winston.debug('no task text, returning null task');
         return null;
      }

      let projectResults;
      while (projectResults = projectsPattern.exec(task.text)) {
         let project = projectResults[1];
         projects.push(project);
      }

      let contextResults;
      while (contextResults = contextsPattern.exec(task.text)) {
         let context = contextResults[1];
         contexts.push(context);
      }

      winston.debug(task);
      return task;
   }


   return {
      create: create,
      parse: parse,
      parseText, parseText,
      save: save
   };

})();
