let winston = require('winston');
let program = require('commander');
let lifelist = require('./list.js');
let render = require('./render.js');

winston.level = 'info';

program
   .version('0.0.1');

program
   .command('list')
   .description('show a todo list')
   .option('-l, --listname <listname>', 'the list name')
   .option('-p, --project <projects>', 'project names')
   .option('-c, --context <contexts>', 'context values')
   .option('-a, --all', 'show all tasks, including completed tasks')
   .action(function(options){
      lifelist.getList({
         listname: options.listname,
         projects: parseList(options.project),
         contexts: parseList(options.context),
         showAll: options.all
      }).then(render.list);
   });

program
   .command('add-task')
   .description('adds a task to a list')
   .option('-l, --listname <listname>', 'the list name')
   .option('-m, --task <todo-txt>', 'todo.txt-formatted text blob')
   .action(function(options){
      lifelist.addTask({
         listname: options.listname,
         text: options.task
      });
   });

program
   .command('projects')
   .description('lists projects')
   .option('-l, --listname <listname>', 'the list name')
   .action(function(options){
      lifelist.listProjects({
         listname: options.listname
      }).then(render.renderArray);
   });

program
   .command('contexts')
   .description('lists contexts')
   .option('-l, --listname <listname>', 'the list name')
   .action(function(options){
      lifelist.listContexts({
         listname: options.listname
      }).then(render.renderArray);
   });

function parseList(value){
   if(!value || value.length==0) { return []; }

   let val = value.split(',');
   return val;
}

program.parse(process.argv);
