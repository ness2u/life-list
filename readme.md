# life-list

# it.
Life is a list of lists; categorized by projects and context; populated with tasks and sorted by priority.

Launch main.js with node.
`node main.js [args]`

Or create an alias;
`alias life='node main.js'`

`life <command> [options]`

```
$ life -h

  Usage: main [options] [command]


  Commands:

    list [options]       show a todo list
    add-task [options]   adds a task to a list
    projects [options]   lists projects
    contexts [options]   lists contexts

  Options:

    -h, --help     output usage information
    -V, --version  output the version number
```


# todo.txt format

`[x [dateCompleted]] [(priority)] [<dateCreated>] <text>`

- x markes completion, and can optionally be followed by a completion date.
- priority is parenthesized, and can be any alpha-numeric value.
- creation date is optional
- text can contain any number of +projects or @contexts.
- dates are in the format of yyyy-mm-dd
