module.exports = (function(){

   function renderList(list) {

      list.sort(function(a,b){
         return a.raw > b.raw ? 1 : -1;
      });

      let output = list.map(function(l) { return l.raw; }).join('\n');
      console.log(output);
   }

   function renderArray(array){
      for(let item of array){
         console.log(item);
      }
   }

   return {
      list: renderList,
      renderArray: renderArray
   };

})();
