module.exports = (function(){
   let fs = require('fs');
   let os = require('os');
   let q = require('q');

   let filename = '.lifelist.config';
   let locationPriority = [
      os.homedir() + '/Dropbox/',
      os.homedir() + '/',
      ''
   ];

   let defaultConfig = {
      listDir: os.homedir() + "/Dropbox/lifelist/"
   };

   function load(){
      let promise = findConfig().then(loadConfig);
      return promise;
   }

   function findConfig(){
      let deferred = q.defer();
      let _findConfig = function (pri){
         let def2 = q.defer();
         if(pri >= locationPriority.length){
            def2.resolve();
         } else {
            let path = locationPriority[pri] + filename;
            fs.exists(path, (exists) => {
               if(exists){
                  def2.resolve(path);
               }else {
                  _findConfig(pri+1).then(function(p){
                     def2.resolve(p);
                  });
               }
            });
         }
         return def2.promise;
      }

      _findConfig(0).then(function(path){ deferred.resolve(path); });
      return deferred.promise;
   }

   function loadConfig(path){
      let deferred = q.defer();

      if (!path){ deferred.resolve(defaultConfig); }
      else {
         fs.readFile(path, 'utf8', function(err, json) {
            if(err) { deferred.reject(err);}

            let config  = JSON.parse(json);
            deferred.resolve(config);
         });
      }
      return deferred.promise;
   }

   return {
      load: load
   };
})();
